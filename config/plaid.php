<?php
declare(strict_types=1);

return [
    'client_id' => env('PLAID_CLIENT_ID'),
    'public_key' => env('PLAID_PUBLIC_KEY'),
    'secret_key' => env('PLAID_SECRET_KEY'),
    'environment' => env('PLAID_ENV', 'sandbox'),
];