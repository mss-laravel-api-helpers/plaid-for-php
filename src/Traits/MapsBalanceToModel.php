<?php
declare(strict_types=1);

namespace MidwestSoftware\Plaid\Traits;

use MidwestSoftware\Plaid\Models\Account\Balance;

trait MapsBalanceToModel
{
    protected function mapBalanceToModel(\stdClass $balance): Balance
    {
        $model = new Balance();
        $model->setAvailable((int) (($balance->available ?? 0) * 100));
        $model->setCurrent((int) (($balance->current ?? 0) * 100));
        $model->setLimit((int) (($balance->limit ?? 0) * 100));

        $currencyCode = $balance->iso_currency_code ?? null;
        if (!$currencyCode) {
            $currencyCode = $balance->unofficial_currency_code ?? null;
        }

        $model->setCurrencyCode($currencyCode ?? '');

        return $model;
    }
}
