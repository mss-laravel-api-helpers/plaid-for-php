<?php
declare(strict_types=1);

namespace MidwestSoftware\Plaid\Traits;

use MidwestSoftware\Plaid\Models\Account\Item\Error;

trait MapsItemErrorToModel
{
    protected function mapItemErrorToModel(\stdClass $itemError): Error
    {
        $model = new Error();
        $model->setErrorType($itemError->error_type ?? '');
        $model->setErrorCode($itemError->error_code ?? '');
        $model->setErrorMessage($itemError->error_message ?? '');
        $model->setDisplayMessage($itemError->display_message ?? '');

        return $model;
    }
}
