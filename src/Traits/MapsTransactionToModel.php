<?php
declare(strict_types=1);

namespace MidwestSoftware\Plaid\Traits;

use MidwestSoftware\Plaid\Models\Account\Account;
use MidwestSoftware\Plaid\Models\Account\Transaction;

trait MapsTransactionToModel
{
    protected function mapTransactionToModel(\stdClass $transaction, ?Account $account = null): Transaction
    {
        $model = new Transaction();

        if ($account && $account->getId() === $transaction->account_id ?? '') {
            $model->setAccount($account);
        }

        $model->setId($transaction->transaction_id ?? '');
        $model->setAccountOwner($transaction->account_owner ?? '');
        $model->setAmount((int) (($transaction->amount ?? 0) * 100));
        $model->setCategory($transaction->category ?? []);
        $model->setCategoryId($transaction->category_id ?? '');
        $model->setDate($transaction->date ?? '');

        $currencyCode = $transaction->iso_currency_code ?? null;
        if (!$currencyCode) {
            $currencyCode = $transaction->unofficial_currency_code ?? null;
        }

        $model->setCurrencyCode($currencyCode ?? '');

        // @TODO: Location
        // @TODO: Payment meta

        $model->setDescriptor($transaction->name ?? '');
        $model->setPending($transaction->pending ?? false);
        $model->setPendingTransactionId($transaction->pending_transaction_id ?? '');
        $model->setTransactionType($transaction->transaction_type ?? '');

        return $model;
    }
}
