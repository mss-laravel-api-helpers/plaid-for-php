<?php
declare(strict_types=1);

namespace MidwestSoftware\Plaid\Traits;

use MidwestSoftware\Plaid\Models\Account\Item;

trait MapsItemToModel
{
    use MapsItemErrorToModel;

    protected function mapItemToModel(\stdClass $item): Item
    {
        $model = new Item();

        $model->setId($item->item_id ?? '');
        $model->setAvailableProducts($item->available_products ?? []);
        $model->setBilledProducts($item->billed_products ?? []);

        $error = $item->error ?? null;
        if ($error) {
            $model->setError($this->mapItemErrorToModel($error));
        }

        $model->setInstitutionId($item->institution_id ?? '');
        $model->setWebhook($item->webhook ?? '');

        return $model;
    }
}
