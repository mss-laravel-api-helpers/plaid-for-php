<?php
declare(strict_types=1);

namespace MidwestSoftware\Plaid\Traits;

use MidwestSoftware\Plaid\Models\Account\Account;

trait MapsAccountToModel
{
    use MapsBalanceToModel;

    protected function mapAccountToModel(\stdClass $account): Account
    {
        $model = new Account();

        $model->setId($account->account_id ?? '');
        $model->setMask($account->mask ?? '');
        $model->setName($account->name ?? '');
        $model->setOfficialName($account->official_name ?? '');
        $model->setType($account->type ?? '');
        $model->setSubType($account->subtype ?? '');

        $balances = $account->balances ?? null;
        if ($balances) {
            $balance = $this->mapBalanceToModel($balances);
            $model->setBalance($balance);
        }

        return $model;
    }
}
