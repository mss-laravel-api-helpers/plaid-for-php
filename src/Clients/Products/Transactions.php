<?php
declare(strict_types=1);

namespace MidwestSoftware\Plaid\Clients\Products;

use MidwestSoftware\Plaid\Contracts\Plaid\TransactionsServiceContract;
use MidwestSoftware\Plaid\Models\Collections\TransactionCollection;
use MidwestSoftware\Plaid\PlaidClient;
use MidwestSoftware\Plaid\Traits\MapsAccountToModel;
use MidwestSoftware\Plaid\Traits\MapsItemToModel;
use MidwestSoftware\Plaid\Traits\MapsTransactionToModel;

class Transactions extends PlaidClient implements TransactionsServiceContract
{
    use MapsAccountToModel, MapsItemToModel, MapsTransactionToModel;

    public function get(string $accessToken, string $startDate, string $endDate, ?array $options = null): TransactionCollection
    {
        $auth = $this->buildAuthArray(true, true);
        $payload = array_merge($auth, [
            'start_date' => $startDate,
            'end_date' => $endDate,
        ]);

        if ($options) {
            $payload['options'] = $options;
        }

        $response = $this->makeRequest('post', '/transactions/get', $payload, $accessToken);
        $data = $this->decodeResponse($response);
        return $this->mapTransactionsToCollection($data);
    }

    protected function mapTransactionsToCollection(\stdClass $data): TransactionCollection
    {
        $accounts = [];
        foreach ($data->accounts ?? [] as $account) {
            $accountModel = $this->mapAccountToModel($account);
            $accounts[$accountModel->getId()] = $accountModel;
        }

        $item = $this->mapItemToModel($data->item ?? (object) []);

        $transactions = [];
        foreach ($data->transactions ?? [] as $transaction) {
            $assocAccount = (array_key_exists($transaction->account_id, $accounts)) ? $accounts[$transaction->account_id] : null;
            $transactions[] = $this->mapTransactionToModel($transaction, $assocAccount);
        }

        $collection = new TransactionCollection();
        $collection->setItem($item);
        $collection->setRequestId($data->request_id ?? '');
        $collection->setItems($transactions);

        return $collection;
    }
}
