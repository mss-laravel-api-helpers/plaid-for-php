<?php
declare(strict_types=1);

namespace MidwestSoftware\Plaid\Contracts\Plaid;

use MidwestSoftware\Plaid\Models\Collections\TransactionCollection;

interface TransactionsServiceContract
{
    public function get(string $accessToken, string $startDate, string $endDate, ?array $options = null): TransactionCollection;
}
