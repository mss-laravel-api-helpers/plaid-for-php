<?php
declare(strict_types=1);

namespace MidwestSoftware\Plaid\Contracts;

use MidwestSoftware\Plaid\Models\Auth\ExchangeResponse;

interface PlaidClientContract
{
    public function exchangePublicToken(string $publicToken): ExchangeResponse;
}
