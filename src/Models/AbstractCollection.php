<?php
declare(strict_types=1);

namespace MidwestSoftware\Plaid\Models;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;

abstract class AbstractCollection implements Arrayable, Jsonable
{
    /**
     * @var string|null
     */
    protected $requestId;

    /**
     * @var array
     */
    protected $items;

    public function __construct(array $items = [])
    {
        $this->items = $items;
    }

    public function count(): int
    {
        return count($this->items);
    }

    public function setItems(array $items): void
    {
        $this->items = $items;
    }

    public function setRequestId(string $requestId): void
    {
        $this->requestId = $requestId;
    }

    public function getRequestId(): ?string
    {
        return $this->requestId;
    }

    public function toArray(): array
    {
        return $this->items ?? [];
    }

    public function toJson($options = 0): string
    {
        return json_encode($this->toArray(), $options);
    }
}
