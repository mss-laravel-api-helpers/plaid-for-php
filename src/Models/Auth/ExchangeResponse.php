<?php
declare(strict_types=1);

namespace MidwestSoftware\Plaid\Models\Auth;

use MidwestSoftware\Plaid\Models\AbstractModel;

class ExchangeResponse extends AbstractModel
{
    /**
     * @var string
     */
    protected $accessToken;

    /**
     * @var string
     */
    protected $itemId;

    public function setAccessToken(string $accessToken): void
    {
        $this->accessToken = $accessToken;
    }

    public function setItemId(string $itemId): void
    {
        $this->itemId = $itemId;
    }

    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    public function getItemId(): string
    {
        return $this->itemId;
    }
}
