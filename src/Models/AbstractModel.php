<?php
declare(strict_types=1);

namespace MidwestSoftware\Plaid\Models;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;

abstract class AbstractModel implements Arrayable, Jsonable
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $requestId;

    public function __construct(array $data = [])
    {
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function setRequestId(string $requestId): void
    {
        $this->requestId = $requestId;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getRequestId(): ?string
    {
        return $this->requestId;
    }

    public function toArray(): array
    {
        $data = [];
        foreach (get_object_vars($this) as $property) {
            $data[$property] = $this->$property;
        }

        return $data;
    }

    public function toJson($options = 0): string
    {
        return json_encode($this->toArray(), $options);
    }
}
