<?php
declare(strict_types=1);

namespace MidwestSoftware\Plaid\Models\Collections;

use MidwestSoftware\Plaid\Models\AbstractCollection;
use MidwestSoftware\Plaid\Models\Account\Item;

class TransactionCollection extends AbstractCollection
{
    /**
     * @var Item
     */
    protected $item;

    public function setItem(Item $item): void
    {
        $this->item = $item;
    }

    public function getItem(): Item
    {
        return $this->item;
    }
}
