<?php
declare(strict_types=1);

namespace MidwestSoftware\Plaid\Models\Account;

use MidwestSoftware\Plaid\Models\AbstractModel;

class Balance extends AbstractModel
{
    /**
     * @var int
     */
    protected $current;

    /**
     * @var int
     */
    protected $available;

    /**
     * @var int
     */
    protected $limit;

    /**
     * @var string
     */
    protected $currencyCode;

    public function setCurrent(int $current): void
    {
        $this->current = $current;
    }

    public function setAvailable(int $available): void
    {
        $this->available = $available;
    }

    public function setLimit(int $limit): void
    {
        $this->limit = $limit;
    }

    public function setCurrencyCode(string $currencyCode): void
    {
        $this->currencyCode = $currencyCode;
    }

    public function getCurrent(): int
    {
        return $this->current ?? 0;
    }

    public function getAvailable(): int
    {
        return $this->available ?? 0;
    }

    public function getLimit(): int
    {
        return $this->limit ?? 0;
    }

    public function getCurrencyCode(): string
    {
        return $this->currencyCode;
    }
}
