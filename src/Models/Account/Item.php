<?php
declare(strict_types=1);

namespace MidwestSoftware\Plaid\Models\Account;

use MidwestSoftware\Plaid\Models\AbstractModel;
use MidwestSoftware\Plaid\Models\Account\Item\Error;

class Item extends AbstractModel
{
    /**
     * @var string
     */
    protected $institutionId;

    /**
     * @var string|null
     */
    protected $webhook;

    /**
     * @var Error
     */
    protected $error;

    /**
     * @var string[]
     */
    protected $availableProducts;

    /**
     * @var string[]
     */
    protected $billedProducts;

    public function setInstitutionId(string $institutionId): void
    {
        $this->institutionId = $institutionId;
    }

    public function setWebhook(string $webhook): void
    {
        $this->webhook = $webhook;
    }

    public function setError(Error $error): void
    {
        $this->error = $error;
    }

    public function setAvailableProducts(array $availableProducts): void
    {
        $this->availableProducts = $availableProducts;
    }

    public function setBilledProducts(array $billedProducts): void
    {
        $this->billedProducts = $billedProducts;
    }

    public function getInstitutionId(): string
    {
        return $this->institutionId;
    }

    public function getWebhook(): string
    {
        return $this->webhook;
    }

    public function getError(): Error
    {
        return $this->error;
    }

    public function getAvailableProducts(): array
    {
        return $this->availableProducts;
    }

    public function getBilledProducts(): array
    {
        return $this->billedProducts;
    }
}
