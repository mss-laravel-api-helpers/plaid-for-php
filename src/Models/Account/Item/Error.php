<?php
declare(strict_types=1);

namespace MidwestSoftware\Plaid\Models\Account\Item;

use MidwestSoftware\Plaid\Models\AbstractModel;

class Error extends AbstractModel
{
    /**
     * @var string
     */
    protected $errorType;

    /**
     * @var string
     */
    protected $errorCode;

    /**
     * @var string
     */
    protected $errorMessage;

    /**
     * @var string
     */
    protected $displayMessage;

    public function setErrorType(string $errorType): void
    {
        $this->errorType = $errorType;
    }

    public function setErrorCode(string $errorCode): void
    {
        $this->errorCode = $errorCode;
    }

    public function setErrorMessage(string $errorMessage): void
    {
        $this->errorMessage = $errorMessage;
    }

    public function setDisplayMessage(string $displayMessage): void
    {
        $this->displayMessage = $displayMessage;
    }

    public function getErrorType(): string
    {
        return $this->errorType;
    }

    public function getErrorCode(): string
    {
        return $this->errorCode;
    }

    public function getErrorMessage(): string
    {
        return $this->errorMessage;
    }

    public function getDisplayMessage(): string
    {
        return $this->displayMessage;
    }
}
