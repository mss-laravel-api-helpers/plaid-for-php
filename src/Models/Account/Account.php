<?php
declare(strict_types=1);

namespace MidwestSoftware\Plaid\Models\Account;

use MidwestSoftware\Plaid\Models\AbstractModel;

class Account extends AbstractModel
{
    /**
     * @var Item
     */
    protected $item;

    /**
     * @var Balance
     */
    protected $balance;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $mask;

    /**
     * @var string|null
     */
    protected $officialName;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $subType;

    /**
     * @var string
     */
    protected $verificationStatus;

    public function setItem(Item $item): void
    {
        $this->item = $item;
    }

    public function setBalance(Balance $balance): void
    {
        $this->balance = $balance;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function setMask(string $mask): void
    {
        $this->mask = $mask;
    }

    public function setOfficialName(string $officialName): void
    {
        $this->officialName = $officialName;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function setSubType(string $subType): void
    {
        $this->subType = $subType;
    }

    public function setVerificationStatus(string $verificationStatus): void
    {
        $this->verificationStatus = $verificationStatus;
    }

    public function getItem(): Item
    {
        return $this->item;
    }

    public function getBalance(): Balance
    {
        return $this->balance;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getMask(): string
    {
        return $this->mask;
    }

    public function getOfficialName(): ?string
    {
        return $this->officialName;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getSubType(): string
    {
        return $this->subType;
    }

    public function getVerificationStatus(): string
    {
        return $this->verificationStatus;
    }
}
