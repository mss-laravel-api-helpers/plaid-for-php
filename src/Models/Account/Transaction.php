<?php
declare(strict_types=1);

namespace MidwestSoftware\Plaid\Models\Account;

use MidwestSoftware\Plaid\Models\AbstractModel;

class Transaction extends AbstractModel
{
    /**
     * @var Account
     */
    protected $account;

    /**
     * @var array|null
     */
    protected $category;

    /**
     * @var string|null
     */
    protected $categoryId;

    /**
     * @var string
     */
    protected $transactionType;

    /**
     * @var string
     */
    protected $descriptor;

    /**
     * @var int
     */
    protected $amount;

    /**
     * @var string|null
     */
    protected $currencyCode;

    /**
     * @var string
     */
    protected $date;

    protected $location;

    protected $paymentMeta;

    /**
     * @var bool
     */
    protected $pending;

    /**
     * @var string|null
     */
    protected $pendingTransactionId;

    /**
     * @var string|null
     */
    protected $accountOwner;

    public function setAccount(Account $account): void
    {
        $this->account = $account;
    }

    public function setCategory(array $categories): void
    {
        $this->category = $categories;
    }

    public function setCategoryId(string $categoryId): void
    {
        $this->categoryId = $categoryId;
    }

    public function setTransactionType(string $transactionType): void
    {
        $this->transactionType = $transactionType;
    }

    public function setDescriptor(string $descriptor): void
    {
        $this->descriptor = $descriptor;
    }

    public function setAmount(int $amount): void
    {
        $this->amount = $amount;
    }

    public function setCurrencyCode(string $currencyCode): void
    {
        $this->currencyCode = $currencyCode;
    }

    public function setDate(string $date): void
    {
        $this->date = $date;
    }

    public function setPending(bool $pending): void
    {
        $this->pending = $pending;
    }

    public function setPendingTransactionId(string $pendingTransactionId): void
    {
        $this->pendingTransactionId = $pendingTransactionId;
    }

    public function setAccountOwner(string $accountOwner): void
    {
        $this->accountOwner = $accountOwner;
    }
}
