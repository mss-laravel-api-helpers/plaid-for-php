<?php
declare(strict_types=1);

namespace MidwestSoftware\Plaid\Providers;

use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class PlaidServiceProvider extends ServiceProvider implements DeferrableProvider
{
    public function register(): void
    {

    }

    public function provides(): array
    {
        return [

        ];
    }
}
