<?php
declare(strict_types=1);

namespace MidwestSoftware\Plaid;

use GuzzleHttp\Client;
use MidwestSoftware\Plaid\Clients\Products\Transactions;
use MidwestSoftware\Plaid\Contracts\PlaidClientContract;
use MidwestSoftware\Plaid\Models\Auth\ExchangeResponse;
use Psr\Http\Message\ResponseInterface;

class PlaidClient implements PlaidClientContract
{
    protected const API_SANDBOX = 'https://sandbox.plaid.com';
    protected const API_DEVELOPMENT = 'https://development.plaid.com';
    protected const API_PRODUCTION = 'https://production.plaid.com';

    protected const API_URLS = [
        'sandbox' => self::API_SANDBOX,
        'development' => self::API_DEVELOPMENT,
        'production' => self::API_PRODUCTION,
    ];

    protected const TOKEN_TYPE_ACCESS = 'access';
    protected const TOKEN_TYPE_PUBLIC = 'public';

    /**
     * @var Client
     */
    protected $httpClient;

    /**
     * @var array
     */
    protected $configuration;

    /**
     * @var string
     */
    protected $apiUrl;

    /**
     * @var Transactions
     */
    protected $transactions;

    public function __construct(Client $httpClient, array $configuration)
    {
        $this->httpClient = $httpClient;
        $this->configuration = $configuration;
        $this->setEnvironment();
    }

    public function exchangePublicToken(string $publicToken): ExchangeResponse
    {
        $response = $this->makeRequest(
            'post',
            '/item/public_token/exchange',
            $this->buildAuthArray(true, true),
            $publicToken,
            static::TOKEN_TYPE_PUBLIC
        );

        $data = $this->decodeResponse($response);

        $model = new ExchangeResponse();
        $model->setRequestId($data->request_id);
        $model->setAccessToken($data->access_token);
        $model->setItemId($data->item_id);

        return $model;
    }

    public function transactions(): Transactions
    {
        if (!$this->transactions instanceof Transactions) {
            $this->transactions = new Transactions($this->httpClient, $this->configuration);
        }

        return $this->transactions;
    }

    protected function makeRequest(string $method, string $endpointUri, array $data, ?string $accessToken, ?string $accessTokenType = 'access'): ResponseInterface
    {
        $split = str_split($endpointUri);
        if ($split[0] !== '/') {
            $endpointUri = '/' . $endpointUri;
        }

        if ($accessToken) {
            switch ($accessTokenType) {
                case static::TOKEN_TYPE_ACCESS:
                    $data['access_token'] = $accessToken;
                    break;
                case static::TOKEN_TYPE_PUBLIC:
                    $data['public_token'] = $accessToken;
                    break;
                default:
                    // throw exception
            }
        }

        return $this->httpClient->request($method, $this->apiUrl . $endpointUri, [
            'json' => $data,
        ]);
    }

    protected function decodeResponse(ResponseInterface $response): \stdClass
    {
        $contents = $response->getBody()->getContents();
        return json_decode($contents);
    }

    protected function buildAuthArray(bool $requireClientId = true, bool $requireSecret = true): array
    {
        $auth = [];
        if ($requireClientId) {
            $auth['client_id'] = $this->configuration['client_id'];
        }

        if ($requireSecret) {
            $auth['secret'] = $this->configuration['secret_key'];
        }

        return $auth;
    }

    protected function setEnvironment(): void
    {
        $configuration = $this->configuration;
        $env = static::API_SANDBOX;

        if (isset($configuration['environment']) && array_key_exists($configuration['environment'], static::API_URLS)) {
            $env = static::API_URLS[$configuration['environment']];
        }

        $this->apiUrl = $env;
    }
}
